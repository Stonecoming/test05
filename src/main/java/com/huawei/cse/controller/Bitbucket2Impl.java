package com.huawei.cse.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2019-01-10T01:48:25.340Z")

@RestSchema(schemaId = "bitbucket2")
@RequestMapping(path = "/cse", produces = MediaType.APPLICATION_JSON)
public class Bitbucket2Impl {

    @Autowired
    private Bitbucket2Delegate userBitbucket2Delegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userBitbucket2Delegate.helloworld(name);
    }

}
